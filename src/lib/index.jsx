import React, { Component } from 'react';
import Button from 'reactstrap/lib/Button';


class ButtonComponent extends Component {

  handleClick = () => {
    this.props.onClick();
  }

  render() {
		const {
			color,
			text,
    } = this.props;
    return (
			<Button onClick={this.handleClick} color={color}>{text}</Button>
    );
  }
}

export default ButtonComponent;
